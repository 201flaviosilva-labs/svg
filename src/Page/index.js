import Assets from "../Assets.js";
import { randomColor, deleteAllChildDom } from "../util.js";
import { links } from "../link.js";

const basePath = "./src/Labs/";

// Dom
const ulList = document.getElementById("ulList");

function main() {
  deleteAllChildDom(ulList);

  const timeSeconds = 10;
  setInterval(updateTitleColor, timeSeconds * 1000);
  updateTitleColor();

  links.map((link, key) => {
    printLink(link, key);
  });
}

function printLink(link, key) {
  const li = document.createElement("li");
  const gameName = document.createElement("h3");
  const WebLink = document.createElement("a");
  const WebImg = document.createElement("img");

  li.style.borderColor = "#7CAE7A";
  li.style.background = "grey";

  li.title = link.name;
  gameName.innerHTML = link.name;

  const divLinks = document.createElement("div");
  divLinks.classList.add("divLinks");

  if (link.web) {
    WebLink.href = basePath + link.web;
    WebImg.src = Assets.Icons.Web;

    WebLink.appendChild(WebImg);
    divLinks.appendChild(WebLink);
  }

  li.appendChild(gameName);
  li.appendChild(divLinks);
  ulList.appendChild(li);
}

main();

function updateTitleColor() {
  document.getElementById("title").style.color = randomColor();
  document.getElementById("footerText").style.color = randomColor();
}

