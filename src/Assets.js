// 	<script src="./script.js" type="module"></script>
// import Assets from "../../Assets.js";

export const assetsPath = "https://bitbucket.org/201flaviosilva/assets/raw/7af8d6ccbcf874ce036f592f0e4f5fa9a68bd1a9";
export const phaserAssetsPath = "https://labs.phaser.io/assets/";

const dinoDead = [
	assetsPath + "/Sprite/Dino/Dead/Dead1.png",
	assetsPath + "/Sprite/Dino/Dead/Dead2.png",
	assetsPath + "/Sprite/Dino/Dead/Dead3.png",
	assetsPath + "/Sprite/Dino/Dead/Dead4.png",
	assetsPath + "/Sprite/Dino/Dead/Dead5.png",
	assetsPath + "/Sprite/Dino/Dead/Dead6.png",
	assetsPath + "/Sprite/Dino/Dead/Dead7.png",
	assetsPath + "/Sprite/Dino/Dead/Dead8.png",
];
const dinoIdle = [
	assetsPath + "/Sprite/Dino/Idle/Idle1.png",
	assetsPath + "/Sprite/Dino/Idle/Idle2.png",
	assetsPath + "/Sprite/Dino/Idle/Idle3.png",
	assetsPath + "/Sprite/Dino/Idle/Idle4.png",
	assetsPath + "/Sprite/Dino/Idle/Idle5.png",
	assetsPath + "/Sprite/Dino/Idle/Idle6.png",
	assetsPath + "/Sprite/Dino/Idle/Idle7.png",
	assetsPath + "/Sprite/Dino/Idle/Idle8.png",
	assetsPath + "/Sprite/Dino/Idle/Idle9.png",
	assetsPath + "/Sprite/Dino/Idle/Idle10.png",
];
const dinoJump = [
	assetsPath + "/Sprite/Dino/Jump/Jump1.png",
	assetsPath + "/Sprite/Dino/Jump/Jump2.png",
	assetsPath + "/Sprite/Dino/Jump/Jump3.png",
	assetsPath + "/Sprite/Dino/Jump/Jump4.png",
	assetsPath + "/Sprite/Dino/Jump/Jump5.png",
	assetsPath + "/Sprite/Dino/Jump/Jump6.png",
	assetsPath + "/Sprite/Dino/Jump/Jump7.png",
	assetsPath + "/Sprite/Dino/Jump/Jump8.png",
	assetsPath + "/Sprite/Dino/Jump/Jump9.png",
	assetsPath + "/Sprite/Dino/Jump/Jump10.png",
	assetsPath + "/Sprite/Dino/Jump/Jump11.png",
	assetsPath + "/Sprite/Dino/Jump/Jump12.png",
];
const dinoRun = [
	assetsPath + "/Sprite/Dino/Run/Run1.png",
	assetsPath + "/Sprite/Dino/Run/Run2.png",
	assetsPath + "/Sprite/Dino/Run/Run3.png",
	assetsPath + "/Sprite/Dino/Run/Run4.png",
	assetsPath + "/Sprite/Dino/Run/Run5.png",
	assetsPath + "/Sprite/Dino/Run/Run6.png",
	assetsPath + "/Sprite/Dino/Run/Run7.png",
	assetsPath + "/Sprite/Dino/Run/Run8.png",
];
const dinoWalk = [
	assetsPath + "/Sprite/Dino/Walk/Walk1.png",
	assetsPath + "/Sprite/Dino/Walk/Walk2.png",
	assetsPath + "/Sprite/Dino/Walk/Walk3.png",
	assetsPath + "/Sprite/Dino/Walk/Walk4.png",
	assetsPath + "/Sprite/Dino/Walk/Walk5.png",
	assetsPath + "/Sprite/Dino/Walk/Walk6.png",
	assetsPath + "/Sprite/Dino/Walk/Walk7.png",
	assetsPath + "/Sprite/Dino/Walk/Walk8.png",
	assetsPath + "/Sprite/Dino/Walk/Walk9.png",
	assetsPath + "/Sprite/Dino/Walk/Walk10.png",
];

// ------------ UI
// https://fonts.google.com/specimen/Press+Start+2P?preview.text_type=custom    // -> Press Start 2

const Assets = {
	Icons: {
		Apple: assetsPath + "/Icons/Appe.svg",
		Linux: assetsPath + "/Icons/Linux.svg",
		Web: assetsPath + "/Icons/Web.svg",
		Windows: assetsPath + "/Icons/Windows.svg",
	},
	Mapa: {
		Platforms: {
			Platform: assetsPath + "/Mapa/Platform/platform.png",
		},
		Skys: {
			Sky: assetsPath + "/Mapa/Sky/sky.png",
		},
	},
	Sprites: {
		Bola: {
			Amarelo: assetsPath + "/Sprite/Bolas/BolaAmarelo.png",
			Azul: assetsPath + "/Sprite/Bolas/BolaAzul.png",
			AzulClaro: assetsPath + "/Sprite/Bolas/BolaAzulClaro.png",
			Branca: assetsPath + "/Sprite/Bolas/BolaBranca.png",
			Rosa: assetsPath + "/Sprite/Bolas/BolaRosa.png",
			Verde: assetsPath + "/Sprite/Bolas/BolaVerde.png",
			Vermelha: assetsPath + "/Sprite/Bolas/BolaVermelha.png"
		},
		Bomb: {
			Bomba: assetsPath + "/Sprite/Bomb/bomb.png",
		},
		Coin: {
			Coin: assetsPath + "/Sprite/Coin/Coin.png",
		},
		Dino: {
			Dead: dinoDead,
			Idle: dinoIdle,
			Jump: dinoJump,
			Run: dinoRun,
			Walk: dinoWalk,
			Sheet: {
				dead: assetsPath + "/Sprite/Dino/Sheet/Dead/Dead.png",
				idle: assetsPath + "/Sprite/Dino/Sheet/Idle/Idle.png",
				jump: assetsPath + "/Sprite/Dino/Sheet/Jump/Jump.png",
				run: assetsPath + "/Sprite/Dino/Sheet/Run/Run.png",
				walk: assetsPath + "/Sprite/Dino/Sheet/Walk/Walk.png",
			},
		},
		Disparo: {
			Fire: assetsPath + "/Sprite/Disparo/Fire.png",
			Flame: assetsPath + "/Sprite/Disparo/Flame.png",
		},
		Dude: {
			Dude: assetsPath + "/Sprite/Dude/dude.png",
		},
		Monstro: {
			Master: assetsPath + "/Sprite/Monstro/Master.png",
		},
		Paddle: {
			Horizontal: assetsPath + "/Sprite/Paddle/PaddleHorizontal.png",
			Vertical: assetsPath + "/Sprite/Paddle/PaddleVertical.png",
		},
		Quadrados: {
			Amarelo: assetsPath + "/Sprite/Quadrados/Amarelo.png",
			Azul: assetsPath + "/Sprite/Quadrados/Azul.png",
			AzulClaro: assetsPath + "/Sprite/Quadrados/AzulClaro.png",
			Branco: assetsPath + "/Sprite/Quadrados/Branco.png",
			Cinzento: assetsPath + "/Sprite/Quadrados/Cinzento.png",
			Preto: assetsPath + "/Sprite/Quadrados/Preto.png",
			Rosa: assetsPath + "/Sprite/Quadrados/Rosa.png",
			Verde: assetsPath + "/Sprite/Quadrados/Verde.png",
			Vermelho: assetsPath + "/Sprite/Quadrados/Vermelho.png",
		},
		Ship: {
			Space: {
				_1: assetsPath + "/Sprite/Ship/Space/Spaceship1.png",
				_2: assetsPath + "/Sprite/Ship/Space/Spaceship2.png",
				_3: assetsPath + "/Sprite/Ship/Space/Spaceship3.png",
				_4: assetsPath + "/Sprite/Ship/Space/Spaceship4.png",
				_5: assetsPath + "/Sprite/Ship/Space/Spaceship5.png",
				_6: assetsPath + "/Sprite/Ship/Space/Spaceship6.png",
				_6Dir: assetsPath + "/Sprite/Ship/Space/Spaceship6Dir.png",
			},
		},
		Star: {
			Star: assetsPath + "/Sprite/Star/star.png",
		},
		Triangulos: {
			Base: assetsPath + "/Sprite/Triangulos/Base.png",
			Black: assetsPath + "/Sprite/Triangulos/Black.png",
			Blue: assetsPath + "/Sprite/Triangulos/Blue.png",
			Green: assetsPath + "/Sprite/Triangulos/Green.png",
			LightBlue: assetsPath + "/Sprite/Triangulos/LightBlue.png",
			Pink: assetsPath + "/Sprite/Triangulos/Pink.png",
			Red: assetsPath + "/Sprite/Triangulos/Red.png",
			White: assetsPath + "/Sprite/Triangulos/White.png",
			Yellow: assetsPath + "/Sprite/Triangulos/Yellow.png",
			GreenRight: assetsPath + "/Sprite/Triangulos/GreenRight.png",
		},
		Zombie: {
			zombie1: assetsPath + "/Sprite/Zombie/Zombie.png",
		}
	},
	UI: {},
	PhaserLabs: {
		spine: {
			"3.8": {
				demos: {
					altlas1: {
						atlas: phaserAssetsPath + "spine/3.8/demos/atlas1.atlas",
						png: phaserAssetsPath + "spine/3.8/demos/atlas1.png",
					},
					altlas2: {
						atlas: phaserAssetsPath + "spine/3.8/demos/atlas2.atlas",
						png: phaserAssetsPath + "spine/3.8/demos/atlas2.png",
					},
					altlas12: {
						png: phaserAssetsPath + "spine/3.8/demos/atlas12.png",
					},
					demos: {
						json: phaserAssetsPath + "spine/3.8/demos/demos.json",
					},
					heroes: {
						atlas: phaserAssetsPath + "spine/3.8/demos/heroes.atlas",
						png: phaserAssetsPath + "spine/3.8/demos/heroes.png",
					},
					heroes2: {
						png: phaserAssetsPath + "spine/3.8/demos/heroes2.png",
					},
				},
				spineBoy: {
					ess: {
						atlas: phaserAssetsPath + "spine/3.8/spineboy/spineboy-ess.atlas",
						json: phaserAssetsPath + "spine/3.8/spineboy/spineboy-ess.json",
						png: phaserAssetsPath + "spine/3.8/spineboy/spineboy-ess.png",
					},
					pro: {
						atlas: phaserAssetsPath + "spine/3.8/spineboy/spineboy-pro.atlas",
						json: phaserAssetsPath + "spine/3.8/spineboy/spineboy-pro.json",
						png: phaserAssetsPath + "spine/3.8/spineboy/spineboy-pro.png",
					},
				}
			},
		},
	}
};

export default Assets;
